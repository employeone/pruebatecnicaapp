//
//  APIHelper.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import Foundation

class APIHelper:NSObject {
    private override init() {
        super.init()
    }
    
    static func getToken() -> Token? {
        //Lectura de archivo de Xcode para autenticar a la API UnSplash (Para mantener en secreto el accesToken)
        guard let url = Bundle.main.url(forResource: "Info.plist", withExtension: nil) else {return nil}
        guard let data = try? Data(contentsOf: url) else { return nil }
        
        return try! PropertyListDecoder().decode(Token.self, from: data)
    }
}

struct Token: Codable {
    var accessToken: String
    enum CodingKeys: String, CodingKey{
         case accessToken = "CLIENT_ID"
    }
}
