//
//  Favoritos.swift
//  EmployOnePrueba
//
//  Created by Aaron on 23/12/20.
//

import Foundation
import CoreData

class Favoritos: NSManagedObject, Identifiable {
    @NSManaged public var image_id: String
    @NSManaged public var alt_image: String
    @NSManaged public var image_url: String
    @NSManaged public var likes: Int32
    @NSManaged public var profileimage: String
    @NSManaged public var user_name: String
}
