//
//  PropertiesList.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import Foundation

struct PropertiesList: Identifiable, Codable {
    var id: String
    var like: Int
    var description: String?
    var alt_description: String?
    var user: User
    var urls: ImageUrl
}

struct User: Codable {
    var id: String
    var username: String?
    var name: String?
    var bio: String?
    var profileimage: ProfileImage
}

struct ProfileImage: Codable {
//    var small: String?
    var medium: String?
//    var large: String?
}

struct ImageUrl: Codable {
    var thumb: String
    var raw: String
    var full: String
}
