//
//  NetworkingManager.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import Foundation
import Alamofire
import SwiftyJSON
import Combine

class NetworkingManager: ObservableObject {
    @Published var propertieslist = [PropertiesList]()
    
    init(){
        guard let accessKey = APIHelper.getToken()?.accessToken, let url = URL(string: "https://api.unsplash.com/photos?client_id=\(accessKey)&page=1&per_page=10&order_by=popular") else {return}
        
        DispatchQueue.main.async {
            AF.request(url).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    for item in json {
                        let id = item.1["id"].string ?? ""
                        let like = item.1["likes"].int ?? 0
                        let description = item.1["id"].string ?? ""
                        let alt_description = item.1["alt_description"].string ?? ""
                        let name = item.1["user"]["name"].string ?? ""
                        let bio = item.1["user"]["bio"].string ?? ""
                        let user_id = item.1["user"]["id"].string ?? ""
                        let username = item.1["user"]["username"].string ?? ""
                        let ProfileImage_medium = item.1["user"]["profile_image"]["medium"].string ?? ""
                        let url_thumb = item.1["urls"]["thumb"].string ?? ""
                        let url_raw = item.1["urls"]["raw"].string ?? ""
                        let url_full = item.1["urls"]["full"].string ?? ""
                        
                        let lista = PropertiesList(id: id, like: like, description: description, alt_description: alt_description, user: User(id: user_id, username: username, name: name,bio: bio, profileimage: ProfileImage(medium: ProfileImage_medium)), urls: ImageUrl(thumb: url_thumb, raw: url_raw, full: url_full))
                        self.propertieslist.append(lista)
                        print(response.result)
                    }
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
