//
//  Home.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct Home: View {
    
    @State var detail : PropertiesList
    
    var body: some View {
        NavigationLink(
            destination: DetailImageView(image_id: detail.id ,user_image: detail.user.profileimage.medium ?? "", bio: detail.user.bio ?? "", name: detail.user.name ?? "", likes: detail.like, alt_description: detail.alt_description ?? "", image_url: detail.urls.full ),
            label: {
                ZStack(alignment: .bottom){
                    AnimatedImage(url: URL(string: detail.urls.full ))
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .clipShape(RoundedRectangle(cornerRadius: 7.0))
                        .shadow(radius: 16)
                    HStack(alignment: .center){
                        AnimatedImage(url: URL(string: detail.user.profileimage.medium ?? ""))
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .clipShape(Circle())
                            .frame(width: 40.0, height: 40.0)
                        VStack{
                            Text(detail.user.username ?? "")
                                .font(.system(size:16, weight:.semibold,design: .rounded))
                            Group{
                                if detail.description != nil{
                                    Text(detail.description ?? "")
                                    .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)
                                    .font(.system(size:14, weight:.regular,design: .rounded))
                                }
                            }
                        }
                        Spacer()
                        Image(systemName: "heart")
                        Text("\(detail.like)")
                    } //HSTACK
                    .padding(.all, 2.0)
                } //Zstack Principal
                .padding(.all, 2.0)
                .foregroundColor(.white)

            })
        }
}

struct Home_Previews: PreviewProvider {
    static let url = "https://images.unsplash.com/photo-1593642632559-0c6d3fc62b89?crop=entropy&cs=srgb&fm=jpg&ixid=MXwxOTI3MDR8MXwxfGFsbHwxfHx8fHx8Mnw&ixlib=rb-1.2.1&q=85"
    static let profileURL = "https://images.unsplash.com/profile-1600096866391-b09a1a53451aimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64"
    
    static let detail = PropertiesList(id: UUID().uuidString, like: 123, description: "Aqui va una descripción", alt_description: "Descripcion de imagen",user: User(id: UUID().uuidString, username: "Mi nombre", name: "Nombre Completo", bio: "Descripcion de Usuario" ,profileimage: ProfileImage(medium: profileURL)), urls: ImageUrl(thumb: url, raw: url, full: url))
    
    
    static var previews: some View {
        Home(detail: detail)
    }
}
