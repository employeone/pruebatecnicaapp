//
//  FinderView.swift
//  EmployOnePrueba
//
//  Created by Aaron on 23/12/20.
//

import SwiftUI

struct FinderHomeView: View {
    @State var expand = false
    @State var search = ""
    @State var page = 1
    @State var isSearching = false
    
    var body: some View {
            HStack{
                if !self.expand{
                          Text("Buscar fotos en Unplash")
                              .font(.caption)
                } //IF EXPAND
                
                Spacer(minLength: 0)
                  
                Image(systemName: "magnifyingglass")
                    .foregroundColor(.gray)
                    .onTapGesture {
                          
                    withAnimation{
                              self.expand = true
                        
                    }
                  }//IMAGE
                
                if self.expand{
                    TextField("Buscar...", text: self.$search)
                    
                    if self.search != "" {
                        
                        Button(action: {
                            if self.isSearching{
                                self.isSearching = false
                            }
                            
                        }) {
                            
                            Text("Buscar")
                                .fontWeight(.bold)
                                .foregroundColor(.black)
                        }
                    }
                    //Cancelar Busqueda
                    Button(action: {
                        withAnimation{
                            self.expand = false
                        }
                        
                        self.search = ""
                        
                        if self.isSearching{
                        }
                        
                    }) {
                        
                        Image(systemName: "xmark")
                            .font(.system(size: 15, weight: .bold))
                            .foregroundColor(.black)
                    }
                    .padding(.leading,10)
                }
                
            }//HSTACK PRINCIPAL
            .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
            .padding()
            .background(Color.white)
        
    }
}

struct FinderHomeView_Previews: PreviewProvider {
    static var previews: some View {
        FinderHomeView()
    }
}
