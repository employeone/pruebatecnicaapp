//
//  ContentView.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct ContentView: View {
    @ObservedObject var manager = NetworkingManager()
    @State private var selection = 0
    
    var body: some View {
        NavigationView {
            VStack (alignment:.trailing){
                FinderHomeView()
                List(self.manager.propertieslist) { list in
                        Home(detail: list)
                }
                .navigationBarTitle("Galería Principal")
            }
        }

        //.edgesIgnoringSafeArea(.top)
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
