//
//  DetailImageView.swift
//  EmployOnePrueba
//
//  Created by Aaron on 23/12/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailImageView: View {
    
    @State private var showAlert: Bool = false
    
    @Environment(\.managedObjectContext) var context
    @Environment(\.presentationMode) var back
    
    var image_id: String
    var user_image: String
    var bio : String
    var name : String
    var likes : Int
    var alt_description: String
    var image_url : String
    
    var body: some View {
        VStack{
            ProfileView(user_image: user_image, bio: bio, name: name, alt_description: alt_description)
            Text("\(likes)")
                .font(.title)
            Text("Likes")
                .font(.title2)
            AnimatedImage(url: URL(string: image_url))
                .resizable()
                .aspectRatio(contentMode: .fit)
                .shadow(radius: 4)
            Spacer()
            Button(action:{
                let favorito = Favoritos(context: self.context)
                favorito.image_id = self.image_id
                favorito.profileimage = self.user_image
                favorito.alt_image = self.alt_description
                favorito.user_name = self.name
                favorito.likes = Int32(self.likes)
                favorito.image_url = self.image_url
                
                do{
                    try self.context.save()
                    self.showAlert.toggle()
                    print("El registro se guardo correctamente")
                    self.back.wrappedValue.dismiss()
                }catch let error as NSError{
                    print("Error al Guardar", error.localizedDescription)
                }
            }){
                    HStack{
                        Image(systemName: "heart.fill")
                            .foregroundColor(.white)
                        Text("Agregar a Favoritos")
                            .foregroundColor(.white)
                            .font(.headline)
                    }
                    .padding(.all)
            }
            .background(Color.blue)
            .cornerRadius(30)
        }.alert(isPresented: $showAlert) { () -> Alert in
            Alert(title: Text("Se guardó a Favoritos correctamente"))}
    }
}

struct DetailImageView_Previews: PreviewProvider {
    static var previews: some View {
        DetailImageView(image_id: UUID().uuidString, user_image: "", bio: "", name: "", likes: 0, alt_description: "",image_url: "")
    }
}
