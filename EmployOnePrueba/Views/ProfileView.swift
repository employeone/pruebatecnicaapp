//
//  ProfileView.swift
//  EmployOnePrueba
//
//  Created by Aaron on 22/12/20.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProfileView: View {
    var user_image: String
    var bio : String
    var name : String
    var alt_description : String
    
    var body: some View {
        
        VStack {
            AnimatedImage(url: URL(string: user_image))
//            Image(systemName: "person.crop.circle.fill")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.blue,lineWidth: 5))
                .shadow(radius: 4)
                .frame(width:100, height: 100)
            Text("\(name)")
                .font(.title)
            Text("\(alt_description)")
                .foregroundColor(.gray)
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(user_image: "", bio: "Biografia del usuario", name: "Aaron Adrian", alt_description: "Aqui es donde va la descripcion de la Foto")
    }
}
